//
// Created by kivi on 17.05.17.
//
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include "ini.h"

const char* get_fullpath(const char *path) {
    if (path[0] == '~') {
        struct passwd *pw = getpwuid(getuid());
        const char *homedir = pw->pw_dir;
        char* fullpath = (char*) malloc(sizeof(char) * strlen(homedir) + strlen(path) - 1);
        memcpy(fullpath, homedir, strlen(homedir));
        memcpy(fullpath + strlen(homedir), path + 1, strlen(path) - 1);
        return fullpath;
    } else return path;
}

typedef struct {
    char* statusbar;
    char* desktop;
} configuration;

configuration config = { NULL, NULL };

static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
    configuration* pconfig = (configuration*)user;

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    if (MATCH("statusbar", "exe")) {
        pconfig->statusbar = get_fullpath(strdup(value));
    } else if (MATCH("desktop", "exe")) {
        pconfig->desktop = get_fullpath(strdup(value));
    } else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}

void init_config(const char* config_file) {
    if (ini_parse(get_fullpath(config_file), handler, &config) < 0) {
        printf("Can't load %s\n", config_file);
        exit(1);
    }
}

const char* get_statusbar() {
    return config.statusbar;
}

const char* get_desktop() {
    return config.desktop;
}