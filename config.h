//
// Created by kivi on 17.05.17.
//

#ifndef XXONWM_CONFIG_H
#define XXONWM_CONFIG_H

const char* get_fullpath(const char *path);

void init_config(const char* config_file);
const char* get_statusbar();
const char* get_desktop();

#endif //XXONWM_CONFIG_H
